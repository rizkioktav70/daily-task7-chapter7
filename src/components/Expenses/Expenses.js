import React, { useState } from 'react';
import ExpenseFilter from './ExpensesFilter';
import ExpenseList from './ExpensesList';
import ExpenseChart from './ExpensesChart';

import Card from '../UI/Card';
import './Expenses.css';


const Expenses = (props) => {
  const [filterByYear, setFilterByYear] = useState("2022")
  const filterChangeHandler = (selectedYear) => {
    setFilterByYear(selectedYear)
  }

  let filteredItem = props.items
  if (filterByYear !== "getall") {
    filteredItem = props.items.filter((item) => {
      return item.date.getFullYear().toString() === filterByYear;
    });
  }

  return (
    <div>
      <Card className='expenses'>
        <ExpenseFilter selected={filterByYear} onChangeFilter={filterChangeHandler} />
        <ExpenseChart expenses={filteredItem} />
        <ExpenseList items={filteredItem} />
      </Card>
    </div>

    // <Card className="expenses">
    //   <ExpenseFilter selected={filterByYear} onChangeFilter={filterChangeHandler} />
    //   {expenseContent}
    //   {/* {filteredItem.length === 0 && <p>No Item found</p>} */}

    //   {/* ternary operator */}
    //   {/* {filteredItem.length === 0 ?
    //     (<p>No Item Found in here B*tch</p>) : (
    //       filteredItem.map((item) => (
    //         <ExpenseItem
    //           key={item.id}
    //           title={item.title}
    //           amount={item.amount}
    //           date={item.date}
    //         />
    //       )
    //       ))
    //   )} */}
    // </Card>
  );
};


export default Expenses;
