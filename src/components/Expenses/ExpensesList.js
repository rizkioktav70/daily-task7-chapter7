import React from "react";

import "./ExpensesList.css"
import ExpenseItem from "./ExpenseItem";

// const ExpensesList = propslet expenseContent = <p>No Expense Found</p>

// if (filteredItem.length > 0) {
//     expenseContent = filteredItem.map((item) => (
//         <ExpenseItem
//             key={item.id}
//             title={item.title}
//             amount={item.amount}
//             date={item.date}
//         />
//     ))
// }

const ExpensesList = props => {
    // let expenseContent = <p className='expenses-list__fallback'>No Expense Found</p>

    if (props.items.length === 0) {
        return <p className='expenses-list__fallback'>No Expense Found</p>
    }

    return (
        <ul className='expenses-list'>
            {props.items.map((item) => (
                <ExpenseItem
                    key={item.id}
                    title={item.title}
                    amount={item.amount}
                    date={item.date}
                />))}
        </ul>
    )
}

export default ExpensesList;
